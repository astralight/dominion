﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AstraDominionAI
{
    public static class Maths
    {
        //amount: 4.3
        // =
        // 1*0.9^0 + 1*0.9^1 + 1*0.9^2 + 1*0.9^3 + 0.3*0.9^4
        public static double Diminish(double amount, double factor = 0.9f)
        {
            double diminishedScore = 0;
            int i;
            for (i = 0; i < amount; i++)
                diminishedScore += 1 * Math.Pow(factor, i);
            diminishedScore += (amount - i) * Math.Pow(factor, i); //remainder
            return diminishedScore;
        }
    }
}
