﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AstraDominionAI
{
    public class Card
    {
        public Card(string n, CardBaseEffect cbe) { name = n; cardBaseEffect = cbe; }
        public String name { get; private set; }
        public CardBaseEffect cardBaseEffect { get; private set; }

    }
    public struct CardBaseEffect
    {
        public int actions;
        public int buys;
        public int treasure;
        public int draws;
        public int victory;
        public int cost;
    }
}
