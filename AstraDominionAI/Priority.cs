﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AstraDominionAI
{
    public class PriorityManager
    {
        Dictionary<Phase, PhasePriorities> phasePriorities = new Dictionary<Phase, PhasePriorities>();
        public PriorityManager()
        {
            phasePriorities.Add(Phase.EARLY, new PhasePriorities());
            phasePriorities.Add(Phase.MID  , new PhasePriorities());
            phasePriorities.Add(Phase.LATE , new PhasePriorities());

            //fake init stuff done here
            phasePriorities[Phase.EARLY].Add(new TreasurePriority());
            phasePriorities[Phase.EARLY].Add(new ActionPriority());
            phasePriorities[Phase.EARLY].Add(new CostPriority());
            phasePriorities[Phase.EARLY].Add(new DrawPriority());
            VictoryPriority vp = new VictoryPriority();
            vp.modifier = -1f;
            phasePriorities[Phase.EARLY].Add(vp);
        }

        public void Score(List<Card> cards, Phase phase)
        {
            Console.WriteLine();
            foreach (Card c in cards) Console.Write(c.name + "\t");
            Console.WriteLine();
            PhasePriorities pp = phasePriorities[phase];
            Console.WriteLine("Score: " + pp.score(cards));
        }

        public Phase DeterminePhase()
        {
            return Phase.EARLY;
        }
        public enum Phase
        {
            EARLY, MID, LATE
        }

    }
    public class PhasePriorities
    {
        List<Priority> priorities = new List<Priority>();
        public void Add(Priority p) { priorities.Add(p); }
        //Scoring is built on the concept that
        // every turn, the average value of each card
        // in a deck should increase each turn.
        //It can't really be enforced, but if the 
        // value decreases occasionally, that means
        // we're not playing correctly.
        public double score(List<Card> deck)
        {
            double score = 0;
            foreach (Priority p in priorities)
            {
                double eScore = 0;
                foreach (Card c in deck)
                    eScore += p.score(c);
                eScore /= deck.Count;
                score += eScore;
                Console.WriteLine(String.Format("{0,20}", p.GetType().Name)+": \t"+eScore);
            }
            return score;
        }
    }
    //Priorities are based off of diminishing returns.
    // In this way, it will prioritize heavily some starting
    // things, like +actions +treasure +cards.
    //However, after a number of those are obtained (and not until then)
    // +victory will start becoming more important.
    //TODO:
    // Perhaps the score should be calculated differently.
    // For instance, score increases as card gets to desired ratio.
    // Doesnt work for victory though, as we need to get another prerequisite first.
    // Plus, at the end, we want different ratios.
    // Might need to do the EARLY-MID-LATE phase strategy.
    public abstract class Priority
    {
        public double score(Card c)
        {
            return modifier * weight * Maths.Diminish(rawScore(c), factor);
        }
        protected abstract double rawScore(Card c);
        public virtual double weight { get { return 1; } }
        public virtual double factor { get { return 0.9d; } }
        public double modifier = 1;
    }
    public class DrawPriority : Priority
    {
        protected override double rawScore(Card c)
        {
            return c.cardBaseEffect.draws;
        }
        public override double weight { get { return 1; } }
        public override double factor { get { return 1d; } }
    }
    public class TreasurePriority : Priority
    {
        protected override double rawScore(Card c)
        {
            return c.cardBaseEffect.treasure;
        }
        public override double weight { get { return 1; } }
        public override double factor { get { return 1d; } }
    }
    public class ActionPriority : Priority
    {
        protected override double rawScore(Card c)
        {
            return c.cardBaseEffect.actions;
        }
        public override double weight { get { return 1; } }
        public override double factor { get { return 0.8d; } }
    }
    public class BuyPriority : Priority
    {
        protected override double rawScore(Card c)
        {
            return c.cardBaseEffect.buys;
        }
        public override double weight { get { return 0.5d; } }
        public override double factor { get { return 0.5d; } }
    }
    public class CostPriority : Priority
    {
        protected override double rawScore(Card c)
        {
            return c.cardBaseEffect.cost;
        }
        public override double weight { get { return 0.5d; } }
        public override double factor { get { return 0.95d; } }
    }
    public class VictoryPriority : Priority
    {
        protected override double rawScore(Card c)
        {
            return c.cardBaseEffect.cost;
        }
        public override double weight { get { return 1d; } }
        public override double factor { get { return 1d; } }
    }
}
