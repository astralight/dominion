﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AstraDominionAI
{
    class Program
    {
        static void Main(string[] args)
        {
            AI ai = new AI();
            ai.ScanCards(new List<String>(new string[] { "copper", "copper", "estate", "estate", "estate" }));
            ai.ScanCards(new List<String>(new string[] { "copper", "copper", "copper", "estate", "estate" }));
            ai.ScanCards(new List<String>(new string[] { "smithy", "estate", "estate", "estate", "estate" }));
            ai.ScanCards(new List<String>(new string[] { "smithy", "smithy", "estate", "estate", "estate" }));
            ai.ScanCards(new List<String>(new string[] { "smithy", "smithy", "smithy", "smithy", "smithy" }));
            ai.ScanCards(new List<String>(new string[] { "village", "village", "village", "village", "village" }));
            ai.ScanCards(new List<String>(new string[] { "smithy", "village", "village", "village", "village" }));
            ai.ScanCards(new List<String>(new string[] { "smithy", "smithy", "village", "village", "village" }));
            ai.ScanCards(new List<String>(new string[] { "smithy", "smithy", "smithy", "village", "village" }));
            ai.ScanCards(new List<String>(new string[] { "smithy", "smithy", "smithy", "smithy", "village" }));
            ai.ScanCards(new List<String>(new string[] { "smithy", "smithy", "smithy", "smithy", "smithy" }));
            Console.ReadKey();
        }
    }
}
