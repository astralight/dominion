﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AstraDominionAI
{
    public static class PhantomGame
    {
        static bool fake = true;
        public static CardBaseEffect Analyze(String cardName)
        {
            return fake ? fakeAnalyze(cardName) : new CardBaseEffect { };
        }
        public static CardBaseEffect fakeAnalyze(String cardName)
        {
            switch(cardName)
            {
                case "copper":
                    return new CardBaseEffect { cost = 0, treasure = 1 };
                case "silver":
                    return new CardBaseEffect { cost = 3, treasure = 2 };
                case "estate":
                    return new CardBaseEffect { cost = 2, victory = 1 };
                case "village":
                    return new CardBaseEffect { cost = 3, actions = 2-1, draws = 1 };
                case "smithy":
                    return new CardBaseEffect { cost = 4, actions = 0-1, draws = 3 };
                default:
                    return new CardBaseEffect { };
            }
        }
    }
}