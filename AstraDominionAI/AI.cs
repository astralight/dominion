﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AstraDominionAI
{
    public class AI
    {
        Dictionary<String, Card> KnownCards = new Dictionary<String, Card>();
        public void ScanCards(List<String> stringCards)
        {
            List<Card> cards = new List<Card>();
            foreach (String cName in stringCards)
            {
                Card c;
                if (!KnownCards.TryGetValue(cName, out c))
                    c = LearnCardBaseEffect(cName);
                cards.Add(c);
            }
            PriorityManager priorityManager = new PriorityManager();
            priorityManager.Score(cards, PriorityManager.Phase.EARLY);
        }
        public Card LearnCardBaseEffect(String cardName)
        {
            Card c = new Card(cardName, PhantomGame.Analyze(cardName));
            KnownCards.Add(cardName, c);
            return c;
        }


        public class Deck
        {

        }
        
    }
}